<?php
    // src/Repository/DiscussionLogRepository.php

    namespace App\Repository;

    use App\Entity\DiscussionLog;
    use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
    use Doctrine\Common\Persistence\ManagerRegistry;

    /**
     * @method DiscussionLog|null find( $id, $lockMode = null, $lockVersion = null )
     * @method DiscussionLog|null findOneBy( array $criteria, array $orderBy = null )
     * @method DiscussionLog[]    findAll()
     * @method DiscussionLog[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
     */
    class DiscussionLogRepository
        extends ServiceEntityRepository
    {
        public function __construct (
            ManagerRegistry $registry
        )
        {
            parent::__construct(
                $registry,
                DiscussionLog::class
            );
        }

        // /**
        //  * @return DiscussionLog[] Returns an array of DiscussionLog objects
        //  */
        /*
        public function findByExampleField($value)
        {
            return $this->createQueryBuilder('d')
                ->andWhere('d.exampleField = :val')
                ->setParameter('val', $value)
                ->orderBy('d.id', 'ASC')
                ->setMaxResults(10)
                ->getQuery()
                ->getResult()
            ;
        }
        */
        /*
        public function findOneBySomeField($value): ?DiscussionLog
        {
            return $this->createQueryBuilder('d')
                ->andWhere('d.exampleField = :val')
                ->setParameter('val', $value)
                ->getQuery()
                ->getOneOrNullResult()
            ;
        }
        */
    }