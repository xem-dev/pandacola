<?php
    // src/Controller/QuestionController.php

    namespace App\Controller;

    use App\Entity\Discussion;
    use App\Entity\DiscussionLog;
    use App\Entity\Question;
    use App\Repository\QuestionRepository;
    use Doctrine\Common\Persistence\ObjectManager;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Symfony\Component\Translation\Loader\YamlFileLoader;
    use Symfony\Component\Translation\Translator;

    /**
     * @Route("/botchat")
     */
    class QuestionController
        extends AbstractController
    {
        /**
         * Translator construct
         *
         * @param string  $locale
         * @param Request $request
         *
         * @return Translator
         */
        public function getTranslator (
            string $locale = 'fr',
            Request $request
        ) : Translator
        {
            $translatorFilePath = realpath( __DIR__ . '/../../translations/botchat+intl-icu.' . $locale . '.yaml' );

            if ( ! $translatorFilePath ) {
                $locale = $request->getLocale();
                $translatorFilePath = realpath( __DIR__ . '/../../translations/botchat+intl-icu.' . $locale . '.yaml' );
            }

            $translator = new Translator( $locale );
            $translator->addLoader( 'yaml', new YamlFileLoader );
            $translator->addResource( 'yaml', $translatorFilePath, $locale );

            return $translator;
        }

        /**
         * Return the answer from the question ID
         *
         * @param int|null           $id
         * @param string             $locale
         * @param Request            $request
         * @param QuestionRepository $questionRepository
         *
         * @return array
         */
        public function getAnswer (
            ?int $id,
            string $locale,
            Request $request,
            QuestionRepository $questionRepository
        ) : array
        {
            $translator = $this->getTranslator( $locale, $request );

            $idParent = null;
            if ( $id ) {
                $questionGivenResult = $questionRepository->find( $id );

                if ( ! $questionGivenResult ) {
                    /*
                     * The ID given is not related to a question => error
                     */
                    $entry = [
                        'type' => 'event',
                        'id' => 'error',
                        'hasParent' => false,
                        'content' => 'error',
                    ];
                } else {
                    /*
                     * Answer translation
                     */
                    foreach ( $questionGivenResult->getTranslations() as $translation ) {
                        if ( $translation->getLocale() == $locale ) {
                            $entry = [
                                'type' => 'log',
                                'id' => $questionGivenResult->getId(),
                                'hasParent' => false,
                                'content' => $translation->getAnswer(),
                            ];
                        }
                    }

                    /*
                     * If there is questions related to this one
                     */
                    if ( count( $questionGivenResult->getQuestions() ) > 0 ) {
                        $idParent = $id;
                        $entry[ 'hasParent' ] = true;
                    }
                }
            } else {
                /*
                 * There is no ID => welcome
                 */
                $entry = [
                    'type' => 'event',
                    'id' => 'welcome',
                    'hasParent' => false,
                    'content' => 'welcome+',
                ];
            }

            return [ $entry, $idParent ];
        }

        /**
         * Give the question choices
         *
         * @param int|null           $idParent
         * @param string             $locale
         * @param Request            $request
         * @param QuestionRepository $questionRepository
         *
         * @return array
         */
        public function getQuestions (
            ?int $idParent,
            string $locale,
            Request $request,
            QuestionRepository $questionRepository
        ) : array
        {
            $array = [];

            $questionsResults = $questionRepository->findBy(
                [
                    'parent' => $idParent,
                ],
                [
                    'orderBy' => 'asc',
                ]
            );

            /*
             * Questions' translation
             */
            foreach ( $questionsResults as $questionsResult ) {
                foreach ( $questionsResult->getTranslations() as $translation ) {
                    if ( $translation->getLocale() == $locale ) {
                        $entry = [
                            'id' => $questionsResult->getId(),
                            'content' => $translation->getQuestion(),
                        ];

                        $array[] = $entry;
                    }
                }
            }

            return $array;
        }

        /**
         * @Route("", name="botchat_index")
         *
         * @return Response
         */
        public function index () : Response
        {
            return $this->render( 'botchat/index.html.twig' );
        }

        /**
         * @Route("/messages", name="botchat_messages")
         *
         * @param Request $request
         *
         * @return Reponse
         */
        public function messages (
            Request $request
        ) : Response
        {
            /*
             * Data settings
             */
            $locale = $request->request->get( 'locale' ) ?? $request->query->get( 'locale' ) ?? $request->getLocale();
            $translator = $this->getTranslator( $locale, $request );

            $array = [
                'back' => $translator->trans( 'back' ),
                'end' => $translator->trans( 'end' ),
                'error' => $translator->trans( 'error' ),
                'other' => $translator->trans( 'other' ),
                'settings' => $translator->trans( 'settings' ),
                'switch' => $translator->trans( 'switch' ),
                'switch+' => $translator->trans( 'switch+' ),
                'welcome' => $translator->trans( 'welcome' ),
                'welcome+' => $translator->trans( 'welcome+' ),
            ];

            // Create JSON
            $json = json_encode( $array );

            /*
             * Give JSON response to Symfony
             */
            $response = new Response;
            $response->setContent( $json );
            $response->headers->set(
                'Content-Type',
                'application/json'
            );

            return $response;
        }

        /**
         * @Route("/dataset", name="botchat_dataset")
         *
         * @param ObjectManager      $objectManager
         * @param QuestionRepository $questionRepository
         *
         * @return Response
         */
        public function dataset (
            ObjectManager $objectManager,
            QuestionRepository $questionRepository
        ) : Response
        {
            /*
             * Data creation
             */
            $array = [
                [
                    'parentId' => null,
                    'orderBy' => 3,
                    'translations' => [
                        'fr' => [
                            'question' => "Comment allez-vous aujourd'hui ?",
                            'answer' => "Je vais bien, merci. Et vous ?.",
                        ],
                        'en' => [
                            'question' => "How are you feeling today ?",
                            'answer' => "I'm fine, thanks. And you ?",
                        ],
                    ],
                ],
                [
                    'parentId' => null,
                    'orderBy' => 1,
                    'translations' => [
                        'fr' => [
                            'question' => "Où se situe l'entreprise ?",
                            'answer' => "À Strasbourg.",
                        ],
                        'en' => [
                            'question' => "Where is the agency located ?",
                            'answer' => "Strasbourg, France.",
                        ],
                    ],
                ],
                [
                    'parentId' => null,
                    'orderBy' => 2,
                    'translations' => [
                        'fr' => [
                            'question' => "Quelles sont vos horaires de travail ?",
                            'answer' => "8h-12h 14h-18h tous les jours ouvrés.",
                        ],
                        'en' => [
                            'question' => "What are your working hours ?",
                            'answer' => "8am-12pm 2pm-6pm every workdays.",
                        ],
                    ],
                ],
                [
                    'parentId' => 1,
                    'orderBy' => 1,
                    'translations' => [
                        'fr' => [
                            'question' => "Bien, merci.",
                            'answer' => "J'en suis heureux.",
                        ],
                        'en' => [
                            'question' => "I'm fine, thanks you.",
                            'answer' => "I'm glad.",
                        ],
                    ],
                ],
                [
                    'parentId' => 1,
                    'orderBy' => 2,
                    'translations' => [
                        'fr' => [
                            'question' => "Pas très bien ...",
                            'answer' => "J'en suis désolé.",
                        ],
                        'en' => [
                            'question' => "Not so well ...",
                            'answer' => "I'm sorry.",
                        ],
                    ],
                ],
                [
                    'parent' => null,
                    'orderBy' => 4,
                    'translations' => [
                        'fr' => [
                            'question' => "Faut-il donner sa chance à Julian ?",
                            'answer' => "Bien sûr !",
                        ],
                        'en' => [
                            'question' => "Should we give it a chance to Julian ?",
                            'answer' => "Of course !",
                        ],
                    ],
                ],
            ];

            foreach ( $array as $entry ) {
                /*
                 * Add question to database
                 */
                // Question creation
                $question = new Question;
                $objectManager->persist( $question );

                // If question has parent, add it
                if ( isset( $entry[ 'parentId' ] ) ) {
                    $questionParent = $questionRepository->find( $entry[ 'parentId' ] );
                    $question->setParent( $questionParent );
                }
                $question->setOrderBy( $entry[ 'orderBy' ] );

                /*
                 * Add translations
                 */
                foreach ( $entry[ 'translations' ] as $locale => $translation ) {
                    $question
                        ->translate( $locale )
                        ->setQuestion( $translation[ 'question' ] )
                        ->setAnswer( $translation[ 'answer' ] );

                    $objectManager->persist( $question );
                }

                /*
                 * Save question and its translations
                 */
                $question->mergeNewTranslations();
                $objectManager->flush();
            }

            return new Response;
        }

        /**
         * @Route("/question", name="botchat_question", methods={"GET|POST"})
         *
         * @param QuestionRepository $questionRepository
         * @param Request            $request
         *
         * @return Response
         */
        public function question (
            QuestionRepository $questionRepository,
            Request $request
        ) : Response
        {
            /*
             * Data settings
             */
            $id = $request->request->get( 'id' ) ?? $request->query->get( 'id' ) ?? null; // Question ID (can be null)
            $locale = $request->request->get( 'locale' ) ?? $request->query->get( 'locale' ) ?? $request->getLocale();

            /*
             * Data
             */
            list( $answer, $idParent ) = $this->getAnswer( $id, $locale, $request, $questionRepository ); // Get answer
            $questions = $this->getQuestions( $idParent, $locale, $request, $questionRepository ); // Get questions (defaults' if answer is not parent, else childrens')
            $array = [
                'answer' => $answer,
                'questions' => $questions,
            ];

            // Create JSON
            $json = json_encode( $array );

            /*
             * Give JSON response to Symfony
             */
            $response = new Response;
            $response->setContent( $json );
            $response->headers->set(
                'Content-Type',
                'application/json'
            );

            return $response;
        }

        /**
         * @Route("/save", name="botchat_save")
         *
         * @param ObjectManager $objectManager
         * @param Request       $request
         *
         * @return Response
         */
        public function save (
            ObjectManager $objectManager,
            Request $request
        ) : Response
        {
            $array = json_decode( $request->request->get( 'discussion' ), true );

            if ( ! empty( $array ) ) {
                /*
                 * Discussion creation
                 */
                $discussion = new Discussion;

                foreach ( $array as $entry ) {
                    /*$dateTimeImmutable = new \DateTimeImmutable( $entry[ 'date' ] );
                    $dateTimeImmutable->__toString = function () {
                        return $this->format( 'Y-m-d H:i:s' );
                    };*/

                    $discussionLog = new DiscussionLog;
                    $discussionLog
                        ->setDiscussion( $discussion )
                        ->setDatetimetz( $entry[ 'date' ] )
                        ->setType( $entry[ 'type' ] )
                        ->setTypeId( $entry[ 'id' ] );
                    $objectManager->persist( $discussionLog );
                    $discussion->addDiscussionLog( $discussionLog );
                }

                $objectManager->persist( $discussion );
                $objectManager->flush();
            }

            return new Response;
        }
    }