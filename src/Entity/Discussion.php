<?php
    // src/Entity/Discussion.php
    namespace App\Entity;

    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\Common\Collections\Collection;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\DiscussionRepository")
     * @ORM\Table(name="discussions")
     */
    class Discussion
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         */
        private $id;
        /**
         * @ORM\OneToMany(targetEntity="App\Entity\DiscussionLog", mappedBy="discussion", orphanRemoval=true)
         */
        private $discussionLogs;

        public function __construct ()
        {
            $this->discussionLogs = new ArrayCollection;
        }

        public function __toString ()
        {
            return $this->id;
        }

        public function getId () : ?int
        {
            return $this->id;
        }

        /**
         * @return Collection|DiscussionLog[]
         */
        public function getDiscussionLogs () : Collection
        {
            return $this->discussionLogs;
        }

        public function addDiscussionLog (
            DiscussionLog $discussionLog
        ) : self
        {
            if ( ! $this->discussionLogs->contains( $discussionLog ) ) {
                $this->discussionLogs[] = $discussionLog;
                $discussionLog->setDiscussion( $this );
            }

            return $this;
        }

        public function removeDiscussionLog (
            DiscussionLog $discussionLog
        ) : self
        {
            if ( $this->discussionLogs->contains( $discussionLog ) ) {
                $this->discussionLogs->removeElement( $discussionLog );
                // set the owning side to null (unless already changed)
                if ( $discussionLog->getDiscussion() === $this ) {
                    $discussionLog->setDiscussion( null );
                }
            }

            return $this;
        }
    }