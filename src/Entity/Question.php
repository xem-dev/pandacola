<?php
    // src/Entity/Question.php

    namespace App\Entity;

    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\Common\Collections\Collection;
    use Doctrine\ORM\Mapping as ORM;
    use Knp\DoctrineBehaviors\Model as ORMBehaviors;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
     * @ORM\Table(name="questions")
     */
    class Question
    {
        use ORMBehaviors\Translatable\Translatable;
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         */
        private $id;
        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Question", inversedBy="questions")
         */
        private $parent;
        /**
         * @ORM\OneToMany(targetEntity="App\Entity\Question", mappedBy="parent")
         */
        private $questions;
        /**
         * @ORM\Column(type="smallint")
         */
        private $orderBy;

        public function __construct ()
        {
            $this->questions = new ArrayCollection;
        }

        public function getId () : ?int
        {
            return $this->id;
        }

        public function getParent () : ?self
        {
            return $this->parent;
        }

        public function setParent ( ?self $parent ) : self
        {
            $this->parent = $parent;

            return $this;
        }

        /**
         * @return Collection|self[]
         */
        public function getQuestions () : Collection
        {
            return $this->questions;
        }

        public function addQuestion ( self $question ) : self
        {
            if ( ! $this->questions->contains( $question ) ) {
                $this->questions[] = $question;
                $question->setParent( $this );
            }

            return $this;
        }

        public function removeQuestion ( self $question ) : self
        {
            if ( $this->questions->contains( $question ) ) {
                $this->questions->removeElement( $question );
                // set the owning side to null (unless already changed)
                if ( $question->getParent() === $this ) {
                    $question->setParent( null );
                }
            }

            return $this;
        }

        public function getOrderBy () : ?int
        {
            return $this->orderBy;
        }

        public function setOrderBy ( int $orderBy ) : self
        {
            $this->orderBy = $orderBy;

            return $this;
        }
    }
