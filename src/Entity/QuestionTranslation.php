<?php
    // src/Entity/QuestionTranslation.php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Knp\DoctrineBehaviors\Model as ORMBehaviors;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\QuestionTranslationRepository")
     * @ORM\Table(name="questions_translations")
     */
    class QuestionTranslation
    {
        use ORMBehaviors\Translatable\Translation;
        /**
         * @ORM\Column(type="string", length=255)
         */
        private $question;
        /**
         * @ORM\Column(type="string", length=255)
         */
        private $answer;

        public function getId () : ?int
        {
            return $this->id;
        }

        public function getQuestion () : ?string
        {
            return $this->question;
        }

        public function setQuestion ( string $question ) : self
        {
            $this->question = $question;

            return $this;
        }

        public function getAnswer () : ?string
        {
            return $this->answer;
        }

        public function setAnswer ( string $answer ) : self
        {
            $this->answer = $answer;

            return $this;
        }
    }
