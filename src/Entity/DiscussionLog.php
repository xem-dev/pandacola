<?php
    // src/EntityDiscussionLog.php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\DiscussionLogRepository")
     * @ORM\Table(name="discussions_logs")
     */
    class DiscussionLog
    {
        /**
         * @ORM\Id()
         * @ORM\ManyToOne(targetEntity="App\Entity\Discussion", inversedBy="discussionLogs")
         * @ORM\JoinColumn(nullable=false)
         */
        private $discussion;
        /**
         * @ORM\Id()
         * @ORM\Column(type="string", length=25)
         */
        private $datetimetz;
        /**
         * @ORM\Column(type="string", length=10)
         */
        private $type;
        /**
         * @ORM\Column(type="string", length=10)
         */
        private $typeId;

        public function getDatetimetz () : ?\DateTimeInterface
        {
            $dateTimeImmutable = new \DateTimeImmutable( $this->datetimetz );

            return $dateTimeImmutable;
        }

        public function setDatetimetz (
            string $datetimetz
        ) : self
        {
            $this->datetimetz = $datetimetz;

            return $this;
        }

        public function getType () : ?string
        {
            return $this->type;
        }

        public function setType (
            string $type
        ) : self
        {
            $this->type = $type;

            return $this;
        }

        public function getDiscussion () : ?Discussion
        {
            return $this->discussion;
        }

        public function setDiscussion (
            ?Discussion $discussion
        ) : self
        {
            $this->discussion = $discussion;

            return $this;
        }

        public function getTypeId () : ?string
        {
            return $this->typeId;
        }

        public function setTypeId (
            string $typeId
        ) : self
        {
            $this->typeId = $typeId;

            return $this;
        }
    }