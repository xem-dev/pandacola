<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190905065745 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, order_by SMALLINT NOT NULL, INDEX IDX_8ADC54D5727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE questions_translations (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, question VARCHAR(255) NOT NULL, answer VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_E33C9B9C2C2AC5D3 (translatable_id), UNIQUE INDEX questions_translations_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE questions ADD CONSTRAINT FK_8ADC54D5727ACA70 FOREIGN KEY (parent_id) REFERENCES questions (id)');
        $this->addSql('ALTER TABLE questions_translations ADD CONSTRAINT FK_E33C9B9C2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES questions (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE questions DROP FOREIGN KEY FK_8ADC54D5727ACA70');
        $this->addSql('ALTER TABLE questions_translations DROP FOREIGN KEY FK_E33C9B9C2C2AC5D3');
        $this->addSql('DROP TABLE questions');
        $this->addSql('DROP TABLE questions_translations');
    }
}
