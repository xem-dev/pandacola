<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190905083139 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE discussions (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discussions_logs (datetimetz VARCHAR(25) NOT NULL, discussion_id INT NOT NULL, type VARCHAR(10) NOT NULL, type_id VARCHAR(10) NOT NULL, INDEX IDX_125B883F1ADED311 (discussion_id), PRIMARY KEY(discussion_id, datetimetz)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discussions_logs ADD CONSTRAINT FK_125B883F1ADED311 FOREIGN KEY (discussion_id) REFERENCES discussions (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE discussions_logs DROP FOREIGN KEY FK_125B883F1ADED311');
        $this->addSql('DROP TABLE discussions');
        $this->addSql('DROP TABLE discussions_logs');
    }
}
