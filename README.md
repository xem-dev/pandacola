# Pandacola exercice (en)

The project was to create a chat bot who acts like a game NPC (you have predefined question choices, the bot answers about it).

### Installation

The project is Symfony 4 based, the installation is done with ``Composer``. Here are the different steps :

- Get the project, of course
- Command : ``composer install``
- Command : ``yarn install``
- Command : ``yarn build``
- Modify ``.env`` to put the right data for the MySQL connection
- Command : ``php bin/console doctrine:database:create``
- Command : ``php bin/console doctrine:migrations:migrate``
- Commande : ``php bin/console server:run``

### Questions creation

A page has been created so you don't have to create the data yourself. Once your server is runned, go to the ``/botchat/dataset`` page. It will return an empty response, don't be scared by the blank page.

### Botchat

The bot chat is on the ``/botchat`` page. It runs with the ``/botchat/question`` page, who returns an answer and the next questions possible. It normally need ``$_POST`` data, but I let this one be questionnable by ``$_GET`` to see the results by yourself. This way, you can test these pages :

- ``/botchat/question``
- ``/botchat/question?id=1``
- ``/botchat/question?id=3``
- ``/botchat/question?id=999999999``
- ``/botchat/question?locale=fr``
- ``/botchat/question?locale=fr&id=1``
- ``/botchat/question?locale=fr&id=3``
- ``/botchat/question?locale=fr&id=999999999``
 
### Settings
 
I added settings to let you choose your language or another theme. The default one is ``panda-cola-light``. Play with those to see the different choices.

---

# Exercice pour Pandacola (fr)

Le projet était de créer un robot de tchat agissant comme un PNJ de jeu vidéo (vous avez des choix prédéfinis, le robot y répond).

### Installation

Le project est basé sur Symfony 4, l'installation se fait avec ``Composer``. Voici les différentes étapes :

- Récupérer le projet, bien entendu
- Commande : ``composer install``
- Commande : ``yarn install``
- Commande : ``yarn build``
- Modifier ``.env`` pour y mettre les bonnes données pour la connexion à MySQL
- Commande : ``php bin/console doctrine:database:create``
- Commande : ``php bin/console doctrine:migrations:migrate``
- Commande : ``php bin/console server:run``

### Création des questions

Une page a été créée pour que vous n'ayez pas besoin de créer les données vous-mêmes. Une fois le server lancé, allez sur la page ``/botchat/dataset``. Elle retournera une réponse vide, ne soyez pas effrayé par la page blanche.

### Botchat

Le tchat se trouve sur la page ``/botchat``. Il fonctionne avec la page ``/botchat/question``, qui retourne une réponse et les prochaines questions possibles. Elle a normalement besoin de données en ``$_POST``, mais j'ai laissé cette dernière questionnable par ``$_GET`` pour voir les résultats par vous-mêmes. De cette manière, vous pouvez tester ces pages :

- ``/botchat/question``
- ``/botchat/question?id=1``
- ``/botchat/question?id=3``
- ``/botchat/question?id=999999999``
- ``/botchat/question?locale=fr``
- ``/botchat/question?locale=fr&id=1``
- ``/botchat/question?locale=fr&id=3``
- ``/botchat/question?locale=fr&id=999999999``
 
### Options

J'ai ajouté des options pour vous permettre de choisir votre langue ou un autre thème. Celui par défaut est ``panda-cola-light``. Amusez-vous avec elles pour voir les différents choix.
