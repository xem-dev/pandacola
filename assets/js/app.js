// assets/js/app.js

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require( '../css/app.scss' );

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require( 'jquery' );

$( document ).ready( function () {
    /*
     * Default settings
     */
    var locale = 'fr';
    var theme = 'botchat-panda-light';
    var classes = {
        messages: 'col m-2 botchat-message'
    };
    var messagesLocale = [];
    $( '#botchat-settings' ).hide();

    classes[ 'message_bot' ] = classes[ 'messages' ] + ' botchat-message-bot';
    classes[ 'message_info' ] = classes[ 'messages' ] + ' botchat-message-info';
    classes[ 'message_user' ] = classes[ 'messages' ] + ' botchat-message-user';

    /*
     * Initialize
     */
    getMessagesLocale();
    setTimeout( function () {
        var data = {
            type: 'event',
            id: 'welcome',
            date: ( new Date() ).toISOString()
        };
        showMessage( classes[ 'message_bot' ], [], 'welcome' );
        addBotChatAnswer( null );
    }, 1500 );

    /*
     * Show/Hide settings menu
     */
    $( document ).on( 'click', '#botchat-settings-button', function () {
        $( '#botchat-settings' ).slideToggle();
    } );

    /*
     * Switch language from locale
     */
    $( document ).on( 'change', '#botchat-locale', function () {
        locale = $( this ).val();
        getMessagesLocale();

        setTimeout( function () {
            var data = {
                type: 'event',
                id: 'switch:' + locale,
                date: ( new Date() ).toISOString()
            };
            showMessage( classes[ 'message_info' ], data, 'switch' );

            var lastId = null;
            $( '.botchat-message-user' ).each( function () {
                if ( $( this ).data( 'id' ) )
                    lastId = $( this ).data( 'id' );
            } );
            addBotChatAnswer( lastId, false );
        }, 500 );
    } );

    /*
     * Change PandaBotChat theme
     */
    $( document ).on( 'change', '#botchat-theme', function () {
        $( '#botchat' )
            .removeClass( theme )
            .addClass( $( this ).val() );
        theme = $( this ).val();
    } );

    /*
     * Stop the current serie of questions and get default choices
     */
    $( document ).on( 'click', '#botchat-back', function () {
        var data = {
            type: 'event',
            id: 'back',
            date: ( new Date() ).toISOString()
        };
        showMessage( classes[ 'message_info' ], data, 'back' );
        addBotChatAnswer( null );
    } );

    /*
     * At the end, we save the entire conversation
     */
    $( document ).on( 'click', '#botchat-end', function () {
        /*
         * End of conversation message
         */
        var data = {
            type: 'event',
            id: 'end',
            date: ( new Date() ).toISOString()
        };
        showMessage( classes[ 'message_info' ], data, 'end' );

        /*
         * JSON creation
         */
        var array = [];
        $( '.botchat-message' ).each( function () {
            if ( $( this ).data( 'id' ) ) {
                array.push(
                    {
                        date: $( this ).data( 'date' ),
                        type: $( this ).data( 'type' ),
                        id: $( this ).data( 'id' )
                    }
                );
            }
        });
        var json = JSON.stringify( array );

        /*
         * Discussion save in database
         */
        var postValues = 'discussion=' + json;
        $.ajax(
            {
                type: 'POST',
                url: '/botchat/save',
                data: postValues,
                success: function ( values ) {
                    /*
                     * Hide settings and choices
                     */
                    $( '#botchat-choices' ).html( '' );
                    $( '#botchat-settings' ).slideUp();
                    $( '#botchat-settings-button' ).hide();
                }
            }
        );
    } );

    /*
     * Choice a question and get back the bot answer
     */
    $( document ).on( 'click', '#botchat-choices button', function () {
        var id = $( this ).data( 'id' );
        showMessage( classes[ 'message_user' ], [], $( this ).text() );
        addBotChatAnswer( id );
    } );

    /*
     * Switch messages to locale
     */
    function getMessagesLocale () {
        var postValues = 'locale=' + locale;

        $.ajax(
            {
                type: 'POST',
                url: '/botchat/messages',
                data: postValues,
                success: function ( data ) {
                    messagesLocale = data;
                    $( '#botchat-settings-button' ).text( messagesLocale.settings );
                }
            }
        );
    }

    /*
     * Show customized message
     */
    function showMessage ( divClass, data, content ) {
        var dataString = '';
        $.each( data, function ( index, value ) {
            dataString += ' data-' + index + '="' + value + '"';
        } );

        if ( messagesLocale[ content ] )
            contentFinale = messagesLocale[ content ];
        else
            contentFinale = content;

        var message = '<div class="' + divClass + '" ' + dataString + '><span>' + contentFinale + '</span></div>';
        $( '#botchat-content' ).append( message );
        setTimeout( function () {
            $( '#botchat-content' )
                .animate( { scrollTop: $( '.botchat-message-bot:last' ).position().top } );
        }, 100 );
    }

    /*
     * Add the question from the user, the bot answer, and the new choices
     */
    function addBotChatAnswer ( id, showOk = true ) {
        var postValues = 'locale=' + locale;
        if ( id )
            postValues += '&id=' + id;

        $.ajax(
            {
                type: 'POST',
                url: '/botchat/question',
                data: postValues,
                success: function ( values ) {
                    /*
                     * Bot answer
                     */
                    if ( showOk ) {
                        var dataBot = {
                            type: values.answer.type,
                            id: values.answer.id,
                            date: ( new Date() ).toISOString()
                        };
                        showMessage( classes[ 'message_bot' ], dataBot, values.answer.content );
                    }

                    /*
                     * New choices
                     */
                    $( '#botchat-choices' ).html( '' );
                    var buttonClass = 'btn w-100 my-1';
                    if ( values.answer.hasParent ) {
                        var content = '<span id="botchat-back" class="' + buttonClass + '" data-id="back">' + messagesLocale.back + '</span>';
                        $( '#botchat-choices' ).append( content );
                    } else if ( $( '.botchat-message-user' ).length > 0 ) {
                        var content = '<span id="botchat-end" class="' + buttonClass + '" data-id="end">' + messagesLocale.end + '</span>';
                        $( '#botchat-choices' ).append( content );
                    }
                    $.each( values.questions, function ( index, value ) {
                        var content = '<button data-id="' + value.id + '" class="' + buttonClass + '">' + value.content + '</button>';
                        $( '#botchat-choices' ).append( content );
                    } );
                }
            }
        );
    }
} );